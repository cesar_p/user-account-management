#!/bin/bash

# Manage user: disabling, deleting, archiving

readonly ARCHIVE_DIR='/archive'


usage(){
  # Display the usage and exit.
  echo "Usage: ${0} [-dra] USER ... [USERN]" >&2
  echo 'Disable a local linux account.' >&2
  echo '  -d  Deletes accounts instead of disabling them.' >&2
  echo '  -r  Removes the home directory associated with the account(s).' >&2
  echo '  -a  Creates an archive of the home directory associated with the account(s).' >&2
  # shellcheck disable=SC2320
  exit 1
}
# Script requires root privileges, if not successfully exit 1, Message to stderr >&2

if [[ "${UID}" -ne 0 ]]
then
  echo "You required sudo privileges to run the script" >&2
  exit 1
fi

# Parse the options
while getopts dra OPTION
do
  case ${OPTION} in
    d) DELETE_USER='true' ;;
    r) REMOVE_OPTION='-r' ;;
    a) ARCHIVE='true' ;;
    ?) usage ;;
  esac
done

# Remove the options while leaving the remaining arguments.
shift "$(( OPTIND - 1 ))"

# If the user doesn't supply at least one argument, we help
NUMBER_OF_USERS="${#}"
if [[ ${NUMBER_OF_USERS} -lt 1 ]]
then
  usage >&2
fi

# Loop through all the usernames supplied as arguments.
for USERNAME in "${@}"
do
  echo "Processing user: ${USERNAME}"
  # Make sure the UID of the account is at least 1000
  USER_ID="$(id -u "${USERNAME}")"
  if [[ ${USER_ID} -lt 1000 ]]
  then
    echo "Cannot delete ${USERNAME} account with UID ${USER_ID}" >&2
    exit 1
  fi

  # Create an archive if requested to do so.
  if [[ "${ARCHIVE}" = 'true' ]]
  then
    # Make sure the ARCHIVE_DIR directory exists
    if [[ ! -d "${ARCHIVE_DIR}" ]]
    then
      echo "Creating ${ARCHIVE_DIR} directory"
      if ! mkdir -p "${ARCHIVE_DIR}"
      then
        echo "The archive directory ${ARCHIVE_DIR} could not be created." >&2
        exit 1
      fi
    fi

    # Archive the user's home directory and move it into the ARCHIVE_DIR
    HOME_DIR="/home/${USERNAME}"
    ARCHIVE_FILE="${ARCHIVE_DIR}/${USERNAME}.tgz"
    if [[ -d "${HOME_DIR}" ]]
    then
      echo "Archiving ${HOME_DIR} to ${ARCHIVE_FILE}"
      if ! tar -zcf "${ARCHIVE_FILE}" "${HOME_DIR}" &>/dev/null
      then
        echo "Could not create ${ARCHIVE_FILE}." >&2
        exit 1
      else
        echo "Archive: ${ARCHIVE_FILE} created"
      fi
    else
      echo "${HOME_DIR} does not exists or is not a directory." >&2
      exit 1
    fi
  fi

# TODO: review userdel option
  # Check if we need to delete the users
  if [[ "${DELETE_USER}" = 'true' ]]
  then
    # Delete the user and check that the user was deleted
    if ! userdel "${REMOVE_OPTION}" "${USERNAME}"
    then
      echo "User ${USERNAME} could not be deleted" >&2
      exit 1
    else
      echo "User ${USERNAME} was deleted."
    fi
  else
    # The account does not need to be deleted: DELETE_USER != 'true'
    if ! chage -E 0 "${USERNAME}"
    then
      echo "The account ${USERNAME} was disabled"
    fi
  fi
done

exit 0